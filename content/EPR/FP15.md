+++
layout='fp15'
title = 'FP15'
date = 2024-10-11T02:49:36-03:00
draft = false
+++

Desde setembro de 2023, atuo como coordenador do subsistema de aerodinâmica. Com isso, tornei-me responsável pelo desenvolvimento e pela gerência do projeto aerodinâmico do FP-15. Este protótipo teve como conceito geral a manutenção do trabalho realizado no FP-14, com alguns necessários aprimoramentos. Devido a problemas relacionados ao "powertrain", este protótipo não competiu na totalidade das provas dinâmicas da 20ª competição da FSAEB, e teve como resultado o 19º lugar geral, apesar de ter apresentado melhor performance que os veículos anteriores nos testes. O projeto aerodinâmico do veículo foi marcado pela refabricação quase completa, em adição a mudanças conservadoras nos aerofólios utilizados nas asas e no formato do assoalho difusor e carenagem. Estima-se que o carro este veículo teve sua força de arrasto em média reduzida em mais de 20%, sem que se observa-se mudanças relevantes no coeficiente de "downforce".