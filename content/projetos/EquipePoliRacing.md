+++
layout = 'epr'
title = 'Equipe Poli Racing'
date = 2024-10-11T02:38:56-03:00
draft = false
+++

A Equipe Poli Racing é a equipe de Fórmula SAE da Escola Politécnica da USP. A Fórmula SAE é uma conceituada competição universitária de veículos de performance do tipo monoposto e "open wheel", que é organizada pela "Society of Automotive Engineers" (SAE) e existe no Brasil desde 2004, contando hoje com grande visibilidade no contexto do desenvolvimento da engenharia automotiva nacional. Fundada em 2008, a EPR já participou de 14 competições nacionais, e teve, em sua história, como principal destaque o desempenho na competição de 2021, na qual obteve o 4º lugar geral, além de diversos pódios ao longo dos anos em provas como SkidPad, Apresentação, Design e Eficiência Energética. Desde 2022, faço parte do subsistema de aerodinâmica da equipe, e tive a oportunidade de participar ativamente, como trainee e como coordenador, dos últimos três protótipos:
