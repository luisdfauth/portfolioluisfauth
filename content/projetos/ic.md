+++
layout='ic'
title = 'Iniciação Científica'
date = 2024-10-10T22:56:23-03:00
draft = false
+++

Mais recentemente, iniciei uma Iniciação Científica sob orientação do prof. dr. Oswaldo Horikawa, em parceria com o Hospital Universitário da USP (HU USP). O projeto consiste no desenvolvimento de um jogo eletrônico do tipo endless runner o qual terá a função de auxiliar na reabilitação de crianças com problemas locomoção.
Nesse contexto, o jogo será conectado a um dispositivo de exercício de equilíbrio corporal, no qual os movimentos do paciente de fisioterapia serão captados e traduzidos em movimentos de um personagem digital, permitindo a reabilitação de forma lúdica por parte do paciente e a coleta de dados técnicos de forma fácil por parte do profissional da saúde encarregado. 
Para o desenvolvimento do protótipo, conversas constantes são feitas com profissionais do HU USP, a fim de que se entenda melhor de que modo o projeto mecatrônico pode melhor servir as demandas da área médica, ressaltando a multidisciplinaridade do projeto e seu importante valor social.
Esta Iniciação Científica atualmente se econtra em sua fase inicial de desenvolvimento, e tem como duração estimada o período de um ano, a partir de setembro de 2024.
Entendo que esta experiência tem muito a acrescentar a minhas capacidades como engenheiro, tanto tecnicamente, na área de coleta e análise de dados, e programação avançada, quanto socialmente, em seu potencial para gerar benefícios em curto prazo para atividades de fisioterapia no Hospital Universitário.