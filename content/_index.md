Bem-vindo à minha página. Meu nome é Luís, sou Graduando em Engenharia Mecatrônica na Escola Politécnica da USP e membro da Equipe Poli Racing de Fórmula SAE. Tenho interesse e experiência nas áreas de engenharia automotiva e controle e automação. 

Neste site, você encontrará informações sobre o passado e o presente de minha trajetória acadêmica e profissional, incluindo:

- Meu currículo
- Meus projetos
- Minhas experiências acadêmicas