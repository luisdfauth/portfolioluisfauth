+++
title = 'Sobre Mim  '
date = 2024-10-10T22:46:16-03:00
draft = false
+++
  

Sou um estudante de engenharia mecatrônica aplicado com interesse pelas áreas automotiva e de controle e automação. Acredito que a engenharia mecatrônica pode inetragir com as mais diversas áreas quando no desenvolvimento de tecnologia. Desse modo, busco ser um profissional versátil para o melhor desenvolvimento de seus projetos em seus mais diferentes contextos. Busco uma posição desafiadora onde possa aplicar e expandir meus conhecimentos em engenharia, contribuindo para o desenvolvimento de soluções inovadoras.
 
---

### Formação Acadêmica
**Escola Politécnica da Universidade de São Paulo** – [São Paulo, São Paulo]  
*Bacharelado em Engenharia Mecatrônica*  
[2022] – [2026]  
- Participação ativa em projetos extracurriculares, especialmente em competições de engenharia e automobilismo.  
- Foco em disciplinas de aerodinâmica, dinâmica de fluidos e design automotivo.
- Iniciação Científica em andamento na área de sistemas com configuração mecatrônica aliados a projeto de sistemas mecânicos.  

---

### Experiência Profissional
**Equipe Poli Racing** – Subsystema de Aerodinâmica  
*Integrante desde 2022*  
- Participação no desenvolvimento de três protótipos de veículos, contribuindo com a concepção e implementação de soluções aerodinâmicas para otimização do desempenho.  
- Colaboração com outros subsistemas da equipe para melhorar a eficiência global do veículo em competições.  
- Realização de testes e simulações computacionais (CFD) para validar os conceitos desenvolvidos.  
- Elaboração de relatórios técnicos e apresentações para competições e reuniões internas.  
- Participação ativa em competições de Fórmula SAE.

Trainee em Aerodinâmica  
[jun-2022] – [set-2023]  
- Desenvolvimento de CAD (Siemens NX)
- Análise de CFD (ANSYS Fluent)
- Aerodinâmica e Mecânica dos Fluidos
- Manufatura com materiais compósitos (laminações com fibra de vidro e carbono)
- Otimização geométrica das asas dianteira e traseira de veículo de FSAE

Coordenador do Subsistema de Aerodinâmica  
[set-2023] – Atual  
- Desenvolvimento de CAD (Siemens NX)
- Análise de CFD (ANSYS Fluent)
- Aerodinâmica e Mecânica dos Fluidos
- Gestão de pessoas e projetos
- Otimização geométrica das asas dianteira e traseira, assoalho difusor e carenagem de veículo de FSAE


---

### Competências Técnicas
- **Softwares de CAD, CAM e CAE**: Siemens NX, ABAQUS, ANSYS Fluent   
- **Programação**: Python, C, html, css  
- **Ferramentas de Simulação** 
- **Análise de Dados e Dinâmica de Fluidos**  
- **Desenvolvimento de Projetos Aerodinâmicos**
- **Desenvolvimento de Projetos Mecatrônicos**

---
### Competências Pessoais
- **Trabalho em equipe**  
- **Liderança em projetos** 
- **Resolução de problemas**  
- **Pensamento analítico**  
- **Gestão de tempo**

---
### Projetos Acadêmicos e Extracurriculares
**Projeto Datalab-Handout**  
- Desenvolvimento de algoritmos de manipulação de bits para operações em ponto flutuante.  
- Foco na otimização de códigos para melhorar a eficiência computacional, usando operações lógicas de baixo nível.

**Jogo de Dinâmica no estilo endless runner**  
- Implementação de um jogo com mecânicas de dinâmica semelhantes ao Subway Surfers, com foco em movimentação contínua e obstáculos em tempo real.

---


### Idiomas
- Português: Nativo  
- Inglês: Avançado

---

### Certificados e Prêmios
- Certificate in Advanced English (Cambridge University Press and Assessment English) - 2020  
- Certificado de Participação da 20ª Competição Fórmula SAE BRASIL (SAE BRASIL) - 2024  
- Medalha de Prata na 16ª OBMEP (Impa) - 2022  
- Medalha de Ouro na ONC 2020 (Ministério da Ciência, Tecnologia e Inovações e Universidade Federal do Piauí) - 2021  
- Medalha de Prata na ONC 2021 (Ministério da Ciência, Tecnologia e Inovações e Universidade Federal do Piauí) - 2021  
- Medalha de Prata na OBA (Sociedade Astronômica Brasileira e Agência Espacial Brasileira) - 2021  